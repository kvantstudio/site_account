<?php

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\user\RoleInterface;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function site_account_form_user_login_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $form['#submit'][] = '_site_account_user_login_form_submit';
}

/**
 * Custom submit handler for the login form.
 */
function _site_account_user_login_form_submit($form, FormStateInterface $form_state) {
  $url = URL::fromRoute('site_account.personal_account');
  $form_state->setRedirectUrl($url);
}

/**
 * Implements hook_theme().
 */
function site_account_theme($existing, $type, $theme, $path) {
  return [
    'site_account_user_login_form' => [
      'render element' => 'form',
      'file' => 'site_account.page.inc',
      'template' => 'site-account-user-login-form',
    ],
    'site_account_personal_account' => [
      'variables' => [],
      'file' => 'site_account.page.inc',
      'template' => 'site-account-personal-account',
    ],
    'site_account_dashboard' => [
      'variables' => ['label' => NULL, 'data' => []],
      'file' => 'site_account.page.inc',
      'template' => 'site-account-dashboard',
    ],
  ];
}

/**
 * Creates permissions for roles.
 */
function site_account_role_permissions() {
  user_role_grant_permissions(RoleInterface::AUTHENTICATED_ID, [
    'site account allow view own account',
    'site account allow edit own account',
  ]);
}

/**
 * Implements hook_ENTITY_TYPE_presave().
 */
function site_account_file_presave(Drupal\file\FileInterface$entity) {
  $uri = $entity->getFileUri();
  if (preg_match("/users/", $uri) && !preg_match("/user-/", $uri)) {
    $filename_postprocessor = \Drupal::service('site_account.filename_postprocessor');
    $new_filename = $filename_postprocessor->process($entity);
    $directory = \Drupal::service('file_system')->dirname($uri);
    $destination = $directory . '/' . $new_filename;
    if ($new_uri = \Drupal::service('file_system')->move($uri, $destination, \Drupal\Core\File\FileSystemInterface::EXISTS_RENAME)) {
      $entity->set('uri', $new_uri);
      $entity->set('filename', \Drupal::service('file_system')->basename($new_uri));
    }
  }
}

/**
 * Implements hook_default_order_information_alter().
 */
function site_account_default_order_information_alter(array &$order_information, Drupal\user\Entity\User$account) {
  $profile = \Drupal::service('site_account.controller')->getUserProfile($account);
  if (!empty($profile)) {
    $order_information += [
      'name' => $profile->fio,
      'phone_mobile' => $profile->field_phone_mobile,
    ];
  }
}
