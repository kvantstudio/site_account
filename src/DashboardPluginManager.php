<?php

namespace Drupal\site_account;

use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides an Dashboard plugin manager for viewing other data on personal account user page.
 */
class DashboardPluginManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/SiteAccountDashboard',
      $namespaces,
      $module_handler,
      'Drupal\site_account\DashboardPluginInterface',
      'Drupal\site_account\Annotation\DashboardPlugin'
    );

    $this->alterInfo('site_account_dashboard_plugin');
    $this->setCacheBackend($cache_backend, 'site_account_dashboard_plugin');
    $this->factory = new DefaultFactory($this->getDiscovery());
  }

}
