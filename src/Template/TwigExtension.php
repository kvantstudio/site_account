<?php

namespace Drupal\site_account\Template;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Twig extension that adds a custom function and a custom filter.
 */
class TwigExtension extends AbstractExtension {

  /**
   * In this function we can declare the extension function
   */
  public function getFunctions() {
    return array(
      new TwigFunction('site_account_user_login_form', array($this, 'siteAccountUserLoginForm')),
    );
  }

  /**
   * Gets a unique identifier for this Twig extension.
   *
   * @return string
   *   A unique identifier for this Twig extension.
   */
  public function getName() {
    return 'site_account.twig_extension';
  }

  /**
   * User authorization form.
   */
  public static function siteAccountUserLoginForm() {
    $form = \Drupal::formBuilder()->getForm("Drupal\site_account\Form\SiteAccountUserLoginForm");
    return $form;
  }
}
