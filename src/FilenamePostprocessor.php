<?php

namespace Drupal\site_account;

use Drupal\Component\Transliteration\TransliterationInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\file\FileInterface;

class FilenamePostprocessor {

  protected $configFactory;
  protected $transliteration;

  public function __construct(ConfigFactoryInterface $config_factory, TransliterationInterface $transliteration) {
    $this->configFactory = $config_factory;
    $this->transliteration = $transliteration;
  }

  /**
   * Формирует новое уникальное имя файла.
   * @param File $entity
   * @return string
   */
  public function process(FileInterface $entity) {
    $filename = $entity->getFilename();
    $extension = $this->get_file_extension($filename);
    $filename = 'user-' . $entity->uuid() . "." . $extension;

    return $filename;
  }

  /**
   * Возвращает расширение файла.
   * @param string $filename
   * @return string|false
   */
  public function get_file_extension(string $filename) {
    return substr(strrchr($filename, '.'), 1);
  }
}
