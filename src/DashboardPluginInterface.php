<?php

namespace Drupal\site_account;

use Drupal\Component\Plugin\PluginInspectionInterface;

interface DashboardPluginInterface extends PluginInspectionInterface {

  /**
   * {@inheritdoc}
   */
  public function getId();

  /**
   * {@inheritdoc}
   */
  public function getLabel();

  /**
   * {@inheritdoc}
   */
  public function getData();

}
