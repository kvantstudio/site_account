<?php

namespace Drupal\site_account\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class SiteAccountRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = parent::getSubscribedEvents();
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -300];
    return $events;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Change path '/user/login' to '/login'.
    if ($route = $collection->get('user.login')) {
      $route->setPath('/login');
    }

    // Change path '/user/register' to '/register'.
    if ($route = $collection->get('user.register')) {
      $route->setPath('/register');
    }

    // Change path '/user/password' to '/reset-password'.
    if ($route = $collection->get('user.pass')) {
      $route->setPath('/reset-password');
    }

    // Prohibiting access to the page.
    if ($route = $collection->get('user.page')) {
      $route->setRequirement('_permission', 'site account allow access user default drupal paths');
    }

    // Prohibiting access to the page.
    if ($route = $collection->get('entity.user.canonical')) {
      $route->setRequirement('_permission', 'site account allow access user default drupal paths');
    }

    // Prohibiting access to the page.
    if ($route = $collection->get('entity.user.edit_form')) {
      $route->setRequirement('_permission', 'site account allow access user default drupal paths');
    }
  }
}
