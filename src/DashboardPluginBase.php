<?php

namespace Drupal\site_account;

use Drupal\Component\Plugin\PluginBase;

abstract class DashboardPluginBase extends PluginBase implements DashboardPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return $this->pluginDefinition['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    $view_label = (bool) $this->pluginDefinition['label'];
    if ($view_label) {
      return $this->pluginDefinition['label'];
    }

    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getData() {
    return [
      '#markup' => 'My data from template.',
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

}
