<?php

namespace Drupal\site_account\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\user\Entity\User;
use Drupal\responsive_image\Entity\ResponsiveImageStyle;

class SiteAccountController extends ControllerBase {

  /**
   * Главная страница личного кабинета.
   */
  public function content() {
    return [
      '#theme' => 'site_account_personal_account',
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * Загружает профиль пользователя.
   */
  public static function getUserProfile($value = NULL) {
    $data = new \stdClass();

    $user = NULL;

    // Пытаемся загрузить пользователя.
    if (empty($value)) {
      $uid = \Drupal::currentUser()->id();
      $user = User::load($uid);
    } else {
      if ($value instanceof User) {
        $user = $value;
      } else {
        if (is_numeric($value)) {
          $user = User::load($value);
        }
      }
    }

    // Если не удалось загрузить пользователя.
    if (!$user) {
      return NULL;
    }

    // Реквизиты доступа.
    $data->uid = $user->id();
    $data->login = $user->get('name')->value;
    $data->email = $user->get('mail')->value;

    // Идентификатор пользователя для отображения.
    if ($data->uid < 10) {
      $data->id = '000' . $data->uid;
    }
    if ($data->uid >= 10 && $data->uid < 100) {
      $data->id = '00' . $data->uid;
    }
    if ($data->uid >= 100 && $data->uid < 1000) {
      $data->id = '0' . $data->uid;
    }
    if ($data->uid >= 1000) {
      $data->id = $data->uid;
    }

    // Изображение пользователя.
    $style = NULL;
    $data->user_picture = NULL;
    $uri = \Drupal::service('kvantstudio.helper')->getMediaUri($user, 'user_picture');
    if ($uri) {
      $data->user_picture_uri = $uri;
      $view_display = \Drupal::entityTypeManager()->getStorage('entity_view_display')->load('user.user.personal_account');
      if ($view_display) {
        $settings = $view_display->getComponent('user_picture')['settings'];
        if (isset($settings['image_style'])) {
          $style = \Drupal::entityTypeManager()->getStorage('image_style')->load($settings['image_style']);
        }
        if (isset($settings['responsive_image_style'])) {
          $style = \Drupal::entityTypeManager()->getStorage('image_style')->load($settings['responsive_image_style']);
        }
      } else {
        $style = \Drupal::entityTypeManager()->getStorage('image_style')->load('large');
      }

      /** @var \Drupal\image\ImageStyleInterface $style */
      if ($style) {
        $data->user_picture = $style->buildUrl($uri);
      } else {
        $data->user_picture = \Drupal::service('file_url_generator')->generateAbsoluteString($uri);
      }
    }

    // ФИО пользователя.
    $data->field_surname = $user->get('field_surname')->value;
    $data->field_name = $user->get('field_name')->value;
    $data->field_patronymic = $user->get('field_patronymic')->value;

    $data->fio = NULL;

    if ($data->field_surname) {
      $data->fio[] = $data->field_surname;
    }

    if ($data->field_name) {
      $data->fio[] = $data->field_name;
    }

    if ($data->field_patronymic) {
      $data->fio[] = $data->field_patronymic;
    }

    if ($data->fio) {
      $data->fio = implode(' ', $data->fio);
    }

    // Контактная информация.
    $data->field_email_office = $user->get('field_email_office')->value;
    $data->field_phone_mobile = $user->get('field_phone_mobile')->value;

    // Прочая информация.
    $data->field_about = !$user->get('field_about')->isEmpty() ? strip_tags($user->get('field_about')->value) : '';

    // Различные настройки.
    $data->field_newsletter = $user->get('field_newsletter')->value;
    $user_data = \Drupal::service('user.data');
    $data->settings = $user_data->get('site_account', $user->id());

    return $data;
  }

  /**
   * Обновляет профиль пользователя.
   */
  public static function updateUserProfile(object $fields, int $uid = 0) {
    if (empty($uid)) {
      $uid = \Drupal::currentUser()->id();
    }

    $user = User::load($uid);

    if ($fields->fid) {
      $user->set('user_picture', $fields->fid);
    }

    $user->field_surname->value = $fields->field_surname;
    $user->field_name->value = $fields->field_name;
    $user->field_patronymic->value = $fields->field_patronymic;
    $user->field_about->value = strip_tags($fields->field_about);
    $user->field_email_office->value = $fields->field_email_office;
    $user->field_phone_mobile->value = $fields->field_phone_mobile;
    $user->field_newsletter->value = $fields->field_newsletter;

    if ($fields->password) {
      $user->setPassword($fields->password);
    }

    $user->save();

    return $user;
  }
}
