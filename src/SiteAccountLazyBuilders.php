<?php

namespace Drupal\site_account;

/** @package Drupal\site_account */
class SiteAccountLazyBuilders {

  /**
   * Возвращает имя текущего пользователя.
   *
   * @return string
   */
  public function getUserName() {
    $account = \Drupal::currentUser();
    return [
      '#markup' => $account->getUsername(),
    ];
  }

}
