<?php

namespace Drupal\site_account\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Annotations for DashboardPlugin.
 *
 * @Annotation
 */
class DashboardPlugin extends Plugin {

  /**
   * The plugin ID.
   */
  public $id;

  /**
   * Label will be used in interface.
   */
  public $label;

  /**
   * Label visability status.
   */
  public $view_label;

}
