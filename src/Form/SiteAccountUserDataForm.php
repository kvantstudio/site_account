<?php

namespace Drupal\site_account\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\site_account\Controller\SiteAccountController as UserAccount;
use Drupal\Component\Utility\Html;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/** @package Drupal\site_account\Form */
class SiteAccountUserDataForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_account_user_data_form';
  }

  /**
   * Идентификтор node.
   *
   * @var int
   */
  protected $uid;

  /**
   * Конструктор формы.
   *
   * @param [type] $nid
   */
  public function __construct(int $uid = 0) {
    $this->uid = $uid;
  }

  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#id'] = Html::getId($this->getFormId());
    $form_class = Html::getClass($form['#id']);

    // Получаем данные пользователя.
    $profile = UserAccount::getUserProfile($this->uid);
    $form['uid'] = [
      '#type' => 'hidden',
      '#value' => $profile->uid,
    ];

    // Реквизиты доступа к сайту.
    $form['register_data'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Details of access'),
      '#attributes' => [
        'class' => [$form_class . '__fieldset'],
      ],
    ];

    $form['register_data']['login'] = [
      '#type' => 'textfield',
      '#title' => $this->t('E-mail for authorization'),
      '#attributes' => [
        'class' => [$form_class . '__item', $form_class . '__login'],
        'placeholder' => '',
      ],
      '#maxlength' => 255,
      '#default_value' => $profile->login,
      '#disabled' => TRUE,
    ];

    $form['register_data']['password'] = [
      '#type' => 'password_confirm',
      '#size' => 25,
      '#description' => $this->t('You can enter a new password to access the site. Passwords must match.'),
      '#attributes' => [
        'class' => [$form_class . '__item', $form_class . '__password'],
        'placeholder' => '',
      ],
    ];

    // Реквизиты доступа к сайту.
    $form['contact_details'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Contact details'),
      '#attributes' => [
        'class' => [$form_class . '__fieldset'],
      ],
    );

    // Фамилия.
    $form['contact_details']['field_surname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your surname'),
      '#attributes' => [
        'class' => [$form_class . '__item', $form_class . '__field-surname'],
        'placeholder' => '',
      ],
      '#maxlength' => 255,
      '#default_value' => $profile->field_surname,
    ];

    // Имя.
    $form['contact_details']['field_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your name'),
      '#attributes' => [
        'class' => [$form_class . '__item', $form_class . '__field-name'],
        'placeholder' => '',
      ],
      '#maxlength' => 255,
      '#default_value' => $profile->field_name,
    ];

    // Отчество.
    $form['contact_details']['field_patronymic'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your patronymic'),
      '#attributes' => [
        'class' => [$form_class . '__item', $form_class . '__field-patronymic'],
        'placeholder' => '',
      ],
      '#maxlength' => 255,
      '#default_value' => $profile->field_patronymic,
    ];

    // Phone.
    $form['contact_details']['field_phone_mobile'] = [
      '#type' => 'tel',
      '#title' => $this->t('Mobile phone'),
      '#attributes' => [
        'class' => [$form_class . '__item', 'site-account-my-data__tel'],
        'placeholder' => '',
      ],
      '#maxlength' => 255,
      '#default_value' => $profile->field_phone_mobile,
    ];

    // E-mail.
    $form['contact_details']['field_email_office'] = [
      '#type' => 'email',
      '#title' => $this->t('E-mail for communication'),
      '#attributes' => [
        'class' => [$form_class . '__item', 'site-account-my-data__email'],
        'placeholder' => '',
      ],
      '#maxlength' => 255,
      '#default_value' => $profile->field_email_office,
    ];

    // О себе.
    $form['contact_details']['field_about'] = [
      '#type' => 'textarea',
      '#title' => $this->t('About oneself'),
      '#attributes' => [
        'class' => [$form_class . '__item', $form_class . '__field-about'],
        'placeholder' => '',
      ],
      '#maxlength' => 255,
      '#default_value' => $profile->field_about,
    ];

    $form['field_image'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Your profile picture'),
      '#upload_location' => $this->getPathUserUploadContactCardImage(),
      '#upload_validators' => [
        'file_validate_extensions' => ['jpeg jpg png gif'],
      ],
    ];

    $form['field_newsletter'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('I agree to receive the newsletter'),
      '#default_value' => $profile->field_newsletter,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#attributes' => ['class' => [$form_class . '__btn', $form_class . '__btn_submit']],
    ];

    $form['#attached']['library'][] = 'site_account/user_data_form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Сохраняем значения введенные в форму.
    $fields = new \stdClass();

    // Сохранение файла.
    $fields->fid = 0;
    $form_file = $form_state->getValue('field_image', 0);
    if (!empty($form_file[0])) {
      $fields->fid = $form_file[0];
    }

    $fields->field_surname = trim($form_state->getValue('field_surname'));
    $fields->field_name = trim($form_state->getValue('field_name'));
    $fields->field_patronymic = trim($form_state->getValue('field_patronymic'));
    $fields->field_about = trim($form_state->getValue('field_about'));
    $fields->field_email_office = trim($form_state->getValue('field_email_office'));
    $fields->field_phone_mobile = trim($form_state->getValue('field_phone_mobile'));
    $fields->field_newsletter = (int) $form_state->getValue('field_newsletter');
    $fields->password = trim($form_state->getValue('password'));

    // Обновление профиля пользователя.
    UserAccount::updateUserProfile($fields);
    \Drupal::messenger()->addMessage(t('Changes successfully saved.'), 'status');

    // Страница личного кабинета.
    $url = Url::fromRoute('site_account.personal_account')->toString();
    $response = new RedirectResponse($url);
    $response->send();
  }

  /**
   * Возвращает текущий путь до папки хранения изображений.
   *
   * @return string
   */
  public function getPathUserUploadContactCardImage() {
    $request_time = \Drupal::time()->getRequestTime();
    $date = \Drupal::service('date.formatter')->format($request_time, 'custom', 'Y-m');
    $path = "public://images/users/" . $date;
    return $path;
  }
}
