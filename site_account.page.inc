<?php

/**
 * @file
 * Contains site_account.page.inc.
 *
 * Page callback for site_account module.
 */

/**
 * Implements template_preprocess_site_account_personal_account().
 */
function template_preprocess_site_account_personal_account(&$variables) {
  // Данные пользователя.
  $variables['user'] = \Drupal::service('site_account.controller')->getUserProfile();

  // Формируем дашборды.
  $variables['dashboards'] = [];
  $plugin_list = \Drupal::service('kvantstudio.helper')->getPluginList('plugin.manager.site_account.dashboard');
  foreach ($plugin_list as $plugin_id => $plugin_label) {
    $plugin = \Drupal::service('plugin.manager.site_account.dashboard')->createInstance($plugin_id);

    // Название дашборда.
    $label = $plugin->getLabel();

    // Данные дашборда.
    $data = $plugin->getData();

    $variables['dashboards'][$plugin_id] = [
      '#theme' => 'site_account_dashboard',
      '#label' => $label,
      '#data' => $data,
      '#attached' => [
        'library' => [
          'site_account/dashboard'
        ],
      ],
    ];

  }
}
